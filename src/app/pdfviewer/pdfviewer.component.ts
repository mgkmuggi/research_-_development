import { Component, OnInit } from '@angular/core';
import { pdfDefaultOptions } from 'ngx-extended-pdf-viewer';

@Component({
  selector: 'app-pdfviewer',
  templateUrl: './pdfviewer.component.html',
  styleUrls: ['./pdfviewer.component.scss']
})
export class PdfviewerComponent implements OnInit {
pdf='./assets/images/file-sample.pdf'
  constructor() {  pdfDefaultOptions.assetsFolder = 'bleeding-edge';
}

  ngOnInit(): void {
  }

}
