import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-skeletonloader',
  templateUrl: './skeletonloader.component.html',
  styleUrls: ['./skeletonloader.component.scss']
})
export class SkeletonloaderComponent implements OnInit {
  contentLoaded = false;

  constructor() { }

  ngOnInit(): void {
    setTimeout(() => {
      this.contentLoaded = true;
    }, 6000);

  }

}
