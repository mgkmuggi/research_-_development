import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SkeletonloaderComponent } from './skeletonloader.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
    {
        path: '',
        component:    SkeletonloaderComponent

    }
];

@NgModule({
  declarations: [
    SkeletonloaderComponent

  ],
  imports: [
    RouterModule.forChild(routes),NgxSkeletonLoaderModule.forRoot(),CommonModule,FormsModule

  ],
  providers: [],
  bootstrap: []
})
export class SkeletonloaderModule { }
