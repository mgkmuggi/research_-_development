import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MatCardModule} from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { QuestionAnswerComponent } from './question-answer.component';

import { CommonModule } from '@angular/common';
const routes: Routes = [
    {
        path: '',
        component: QuestionAnswerComponent
    }
];

@NgModule({
  declarations: [
    QuestionAnswerComponent
  ],
  imports: [
    RouterModule.forChild(routes),FormsModule,CommonModule,NgbModule,MatCardModule,MatIconModule,MatTableModule,MatInputModule,MatFormFieldModule
  ],
  providers: [],
  bootstrap: []
})
export class QuestionAnswerModule { }
