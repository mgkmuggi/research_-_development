import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { iif } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'R&D Work';
  topic=[{"type":"1","Name":"Grid and List","image":"./assets/images/gris&list.png"},{"type":"2","Name":"Skeleton Loader","image":"./assets/images/ngxloader.jpeg"},
  {"type":"3","Name":"PDF Viewer","image":"./assets/images/pdfviewer.png"},  {"type":"4","Name":"Reveal JS","image":"./assets/images/q&a.jpg"}]
  constructor(private route:Router){

  }
  ngOnInit(): void {

  }
choosen(id,index){
  console.log(id)
if(id ==1 ){
this.route.navigate(['/gridlist'])
}else if(id ==2){
  this.route.navigate(['/skeleload'])
}else if(id == 3){
  this.route.navigate(['/pdfviewer'])

}else{
 this.route.navigate(['/QA'])

}
}
}
