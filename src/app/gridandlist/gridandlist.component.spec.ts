import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridandlistComponent } from './gridandlist.component';

describe('GridandlistComponent', () => {
  let component: GridandlistComponent;
  let fixture: ComponentFixture<GridandlistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridandlistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridandlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
