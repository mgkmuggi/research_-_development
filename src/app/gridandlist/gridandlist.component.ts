import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gridandlist',
  templateUrl: './gridandlist.component.html',
  styleUrls: ['./gridandlist.component.scss']
})
export class GridandlistComponent implements OnInit {
  grid = true;
  list=true
  dataSource:any;
  displayedColumns: string[] = ['Image','name'];
  contentLoaded = false;

  // name = ['Rajesh', 'Sanjay', 'Vijay','Rajkumar','Karthick','Joyson'];
  name=[{"id":"1","Name":"Rajesh","image":"./assets/images/Rajesh.jpg","post":"Managar","projecttotal":"10","experience":"13"},
  {"id":"2","Name":"Sanjay","image":"./assets/images/Sanjay.jpg","post":"Managar","projecttotal":"15","experience":"12"},
  {"id":"3","Name":"Vijay","image":"./assets/images/VijayShankar.jpg","post":"Vice President","projecttotal":"16","experience":"14"},
  {"id":"4","Name":"Senthil Nathan","image":"./assets/images/Senthilnathan.jpg","post":"Team Leader","projecttotal":"10","experience":"7"},
  {"id":"5","Name":"Mugesh","image":"./assets/images/profileno.jpg","post":"Web Developer/Programmer","projecttotal":"6","experience":"2"},
  {"id":"6","Name":"Sarranya","image":"./assets/images/profileno.jpg","post":"Web Developer/Programmer","projecttotal":"2","experience":"2"},
  {"id":"7","Name":"Venkatachalapathy","image":"./assets/images/profileno.jpg","post":"Web Developer/Programmer","projecttotal":"2","experience":"2"},

]

  constructor() { }

  ngOnInit(): void {
    this.dataSource=this.name;
    setTimeout(() => {
      this.contentLoaded = true;
    }, 2000);

  }

}
