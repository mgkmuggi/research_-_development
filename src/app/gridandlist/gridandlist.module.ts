import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MatCardModule} from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { GridandlistComponent } from './gridandlist.component';
import { CommonModule } from '@angular/common';
const routes: Routes = [
    {
        path: '',
        component: GridandlistComponent
    }
];

@NgModule({
  declarations: [
    GridandlistComponent,
  ],
  imports: [
    RouterModule.forChild(routes),FormsModule,CommonModule,NgbModule,NgxSkeletonLoaderModule,MatCardModule,MatIconModule,MatTableModule,MatInputModule,MatFormFieldModule
  ],
  providers: [],
  bootstrap: []
})
export class GridListModule { }
