import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GridandlistComponent } from './gridandlist/gridandlist.component';

const routes: Routes = [
  {
    path: 'gridlist',
    loadChildren: () =>
      import('./gridandlist/gridandlist.module').then((m) => m.GridListModule),
  },
  {
    path: 'skeleload',
    loadChildren: () =>
      import('./skeletonloader/skeletonloader.module').then((m) => m.SkeletonloaderModule),
  },
  {
    path: 'pdfviewer',
    loadChildren: () =>
      import('./pdfviewer/pdfviewer.module').then((m) => m.PdfViewerModule),
  },
  {
    path: 'QA',
    loadChildren: () =>
      import('./question-answer/question-answer.module').then((m) => m.QuestionAnswerModule),
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
